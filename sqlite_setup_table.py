import sqlite3

# This is not properly cleaned and refactored file as was created only to setup and test SQLite db

try:

    # Connect to DB and create a cursor
    sqliteConnection = sqlite3.connect("cs_sql.db")
    cursor = sqliteConnection.cursor()
    print("DB Init")

    # Write a query and execute it with cursor
    query_version_test = "select sqlite_version();"
    query_users_table_creation = """CREATE TABLE IF NOT EXISTS users(
                username VARCHAR(25) NOT NULL PRIMARY KEY,
                password VARCHAR(255) NOT NULL,
                role VARCHAR(25) NOT NULL,
                messages TEXT
                );"""

    cursor.execute(query_users_table_creation)

    query_messages_table_creation = """CREATE TABLE IF NOT EXISTS messages(
                username VARCHAR(25) NOT NULL,
                message TEXT
                );"""

    cursor.execute(query_messages_table_creation)

    # query_insert_into_users = """INSERT INTO users VALUES (
    #                             'user1',
    #                             '$2b$12$kGaAZ8QfXY/GqfeiIQjnAehFdonoAv3ACnLDh1hiRrRcABD9H6Oy.',
    #                             'user', 'Msg1');"""
    # query_insert_into_users = """INSERT INTO users VALUES (
    #                         'admin',
    #                         '$2b$12$oALBUYC0IbvgiTrAI8IPjuuYqpymb7wLWQuWkyxrUOGbgSf7RQeHW',
    #                         'admin', 'Msg1');"""
    # cursor.execute(query_insert_into_users)

    # query_insert_into_messages = """INSERT INTO messages VALUES (
    #                         'user',
    #                         'Msg1');"""

    # cursor.execute(query_insert_into_messages)
    sqliteConnection.commit()

    # Query data
    query_all_from_users = "SELECT * FROM messages;"
    cursor.execute(query_all_from_users)
    result = cursor.fetchall()
    print(result)

# Handle errors
except sqlite3.Error as error:
    print("Error occurred - ", error)
    raise

# Close DB Connection irrespective of success
# or failure
finally:

    if sqliteConnection:
        sqliteConnection.close()
        print("SQLite Connection closed")
