# echo-server.py

import socket
import json
import time
import bcrypt
import psycopg2
from config import load_config


class Server:
    """Handles server start, commands and client connections"""

    def __init__(self):
        self.addr = "127.0.0.1"
        self.port = 12345
        self.serv_version = "0.3.0"
        self.server_start_time = time.time()
        self.server_creation_date = time.ctime()
        self.config = load_config()
        self.server_start()  # To be commented out while testing

    def server_commands(self, command_msg):
        """Method that checks received command and reacts accordingly"""
        self.command = json.loads(command_msg.decode())
        match self.command["command"]:
            # Server basic commands
            case "uptime":
                self.response = (
                    f"Uptime: {round(time.time() - self.server_start_time,2)} seconds"
                )
            case "info":
                self.response = f"Server version: {self.serv_version}, Creation date: {self.server_creation_date}"
            case "help":
                self.response = (
                    "\n\nCommands list:\n\n"
                    + "uptime - Returns server uptime\n"
                    + "info - Shows server version and start date\n"
                    + "help - Shows list of available commands\n"
                    + "stop - Stops server and client\n"
                    + "login - Input username and password to log in"
                )
            case "stop":
                try:
                    self.stop_response = {"command": "Server is stopping"}
                    self.conn.send(json.dumps(self.stop_response).encode())
                except Exception as e:
                    print(f"Error while sending stop response to client: {e}")
                finally:
                    self.server_stop()
            case "Hello Server! Client here!":
                self.response = "Hello Client! Server here!"
            case "login":
                login_response = self.user_login_db(self.command)
                self.response = {
                    "command": login_response[0],
                    "status": login_response[1],
                    "role": login_response[2],
                    "username": login_response[3],
                }
            case "logged":
                self.response = "Logged successful"
            case "send_msg":
                is_received = self.save_message_db(self.command)
                self.response = is_received
            case "show_data":
                self.response = self.show_data_db(self.command)
            case _:
                self.response = "Unknown command"
        if type(self.response) == dict:
            return json.dumps(self.response).encode()
        else:
            return json.dumps({"command": self.response}).encode()

    def server_start(self):
        """Starting server and waiting to client connection to make handshake"""
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((self.addr, self.port))
        self.socket.listen()
        print(f"Server is listening on address {self.addr} and port {self.port}")
        self.conn, self.client_addr = self.socket.accept()
        print(f"Client {self.client_addr} is connected.")

        while True:
            try:
                # Receive data from client
                print("Waiting to receive data from client...")  # Debugging
                command_msg = self.conn.recv(1024)  # Receive raw bytes
                if command_msg:
                    print(f"Data received: {command_msg}")  # Debugging
                    self.send_response(command_msg)
                else:
                    print("Client disconnected.")
                    break  # Exit inner loop if no data (client disconnected)
            except json.JSONDecodeError:
                print("Received non-JSON data or decoding error.")
            except ConnectionResetError:
                print("Client connection was reset.")
                break

        # Close the connection after client disconnects
        self.conn.close()

    def send_response(self, msg):
        """Method takes json message as argument and sends response according to server commands"""
        response = self.server_commands(msg)
        self.conn.send(response)

    def server_stop(self):
        """The method for safe server closing"""
        self.conn.close()
        self.socket.close()
        print("Server shutting down...")
        exit()

    def pull_user_data_from_db(self, username):
        """
        Retrieve user-specific data from the database.

        Args:
            username (str): The username of the user whose data is to be retrieved.

        Returns:
        tuple: A tuple containing the user's username, password, and role if found,
        or None if the user does not exist.
        """

        try:
            with psycopg2.connect(**self.config) as conn:
                print("Connected to the PostgreSQL server.")
                # Retreiving data from database
                with conn.cursor() as cur:
                    cur.execute(
                        "SELECT username, password, role FROM users WHERE username = %s",
                        (username,),
                    )
                    row = cur.fetchone()
                    return row
        except psycopg2.DatabaseError as db_error:
            print(f"Database error occurred: {db_error}")
            return None
        except (psycopg2.DatabaseError, Exception) as error:
            print(error)

    def pull_whole_data_from_db(self):
        """
        Retrieve all user data from the database.

        Returns:
        list: A list of tuples where each tuple contains a row of user data
        (e.g., user_id, username, password, role, messages), or an empty list if no data is found.
        """

        try:
            # connecting to the PostgreSQL server
            with psycopg2.connect(**self.config) as conn:
                print("Connected to the PostgreSQL server.")
                # Retreiving data from database and listing all users data
                with conn.cursor() as cur:
                    cur.execute("SELECT * FROM users")
                    all_data = cur.fetchall()
                    if not all_data:
                        print("No data found in the 'users' table.")
                return all_data
        except (psycopg2.DatabaseError, Exception) as error:
            print(error)

    def check_inbox_cap_db(self, username):
        """
        Check the number of messages in a user's inbox (messages column in PSQL).

        Args:
            username (str): The username of the user whose inbox capacity is to be checked.

        Returns:
        int: The number of messages in the user's inbox. Returns 0 if the user has no messages
        or None if the user does not exist.
        """

        try:
            with psycopg2.connect(**self.config) as conn:
                print("Connected to the PostgreSQL server.")
                # Retreiving data from database
                with conn.cursor() as cur:
                    cur.execute(
                        "SELECT array_length(messages, 1) FROM users WHERE username = %s",
                        (username,),
                    )
                    row = cur.fetchone()
                    return row

        except psycopg2.DatabaseError as db_error:
            print(f"Database error occurred while checking inbox capacity: {db_error}")
            return None
        except Exception as error:
            print(f"An unexpected error occurred: {error}")
            return None

    def add_message_to_inbox_db(self, username, message):
        """
        Append a message to a user's inbox (messages column in PSQL).

        Args:
            username (str): The name of the user to which inbox the message should be appended.
            message (str): The message to be appended to the user's inbox.

        Returns:
        None
        """

        with psycopg2.connect(**self.config) as conn:
            print("Connected to the PostgreSQL server.")
            # Retreiving data from database
            with conn.cursor() as cur:
                cur.execute(
                    "UPDATE users SET messages = array_append(messages, %s) WHERE username = %s;",
                    (message, username),
                )

    def user_login_db(self, login_command):
        """
        Check login credentials against the database and log the user in if valid.

        Args:
            login_command (dict): A dictionary containing 'username' and 'password' keys.

        Returns:
            tuple: A tuple with a status message, login result ('success' or 'failed'),
                user role, and username if login is successful.
        """

        username = login_command["username"]
        password = login_command["password"]

        # Check if login command received
        if login_command:
            print(f"User data received: {login_command}")
            try:
                db_row = self.pull_user_data_from_db(username)
                while db_row is not None:
                    db_username = db_row[0]
                    db_password = db_row[1]
                    db_role = db_row[2]
                    if username == db_username:
                        if bcrypt.checkpw(
                            password.encode(),
                            db_password.encode(),
                        ):
                            return (
                                f"Login successful. Welcome, {username}!",
                                "success",
                                db_role,
                                username,
                            )
                        else:
                            return (
                                "Invalid password. Please try again.",
                                "failed",
                                "na",
                                "na",
                            )
                return "Username not found.", "failed", "na", "na"
            except (psycopg2.DatabaseError, Exception) as error:
                print(error)

    def save_message_db(self, message_response):
        """
        Save a message to the database, enforcing the user's inbox capacity.

        Args:
            message_response (dict): A dictionary containing 'username' and 'message' keys.

        Returns:
            str: A status message indicating the result of the operation (success, inbox full, or error).
        """

        try:
            message_user = message_response["username"]
            message_text = message_response["message"]

            # Check input values
            if not message_user or not message_text:
                return "Invalid message data: username or message missing"

            # Define inbox limit
            INBOX_LIMIT = 5

            # Check messages in inbox of the user
            msg_count = self.check_inbox_cap_db(message_user)
            print(msg_count)
            if msg_count is None:
                return f"Error: User '{message_user}' not found in the database"

            if int(msg_count[0]) < INBOX_LIMIT:
                self.add_message_to_inbox_db(message_user, message_text)
                return "Message send successfuly"
            else:
                return "User inbox is full, try again later"
        except Exception as e:
            return f"An error occurred: {str(e)}"

    def show_data_db(self, message_response):
        """
        Retrieve data from the database based on the user's role.

        Args:
            message_response (dict): A dictionary containing 'username' and 'role' keys.

        Returns:
            tuple or list: User data as a tuple if the role is 'user', or all user data as a list if the role is 'admin'.
        """

        username = message_response["username"]
        role = message_response["role"]

        if not username or not role:
            print(
                "Invalid input: 'username' or 'role' is missing from message_response."
            )
            return None

        try:
            if role == "user":
                db_row = self.pull_user_data_from_db(username)
                if db_row:
                    print(f"Data retrieved for user '{username}': {db_row}")
                    return db_row
                else:
                    print(f"No data found for user '{username}'.")
                    return None
            elif role == "admin":
                db_string = self.pull_whole_data_from_db()
                if db_string:
                    print("All user data retrieved successfully.")
                    return db_string
                else:
                    print("No data found in the database.")
                    return None
        except Exception as error:
            print(f"An unexpected error occurred: {error}")


if __name__ == "__main__":
    server = Server()
    server.server_start()
