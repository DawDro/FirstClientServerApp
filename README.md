# Client-Server Application

This project is a milestone in the Python Mentoring Program, designed to prepare new programmers for a developer career. The application features custom commands and communication between users, developed in multiple stages.

## Table of Contents
- [Project Overview](#project-overview)
- [Project Stages](#project-stages)
- [Installation](#installation)
- [Usage](#usage)
- [Testing](#testing)
- [Learning Principles](#learning-principles)

## Project Overview

The Client-Server application allows multiple users to connect and communicate with each other. The project evolves through different stages, each adding new functionalities and improvements.

## Project Stages

### Stage 1: Initial Setup [✔]
- Create a simple client and server application without using any framework.
- Implement basic commands and responses in JSON:
  - `uptime` - Returns server uptime.
  - `info` - Shows server version and start date.
  - `help` - Shows a list of available commands.
  - `stop` - Stops the server and client.

### Stage 2: Object-Oriented Programming (OOP) [✔]
- Refactor the code to use OOP principles.

### Stage 3: User Login and Messaging [✔]
- Implement user login functionality.
- Enable sending messages between users.

### Stage 4: User Roles [✔]
- Implement two user roles with different permissions:
  - **Admin** - Full access.
  - **User** - Limited to viewing only their own information.

### Stage 5: Data Storage in JSON [✔]
- Store user data and offline chat history (up to 5 messages) in a JSON file.

### Stage 6: SQL Database Integration [✔]
- Transition from JSON file storage to an SQL database.

### Stage 7: Unittest learnig for server code [✔]
- Create unittests for server code.

### Stage 8: Refactor code for SQLite database [✔]
- Transition from PostgreSQL to SQLite database
  (Duplicated echo_server.py file and refactored it into SQLite as echo_server_sqlite.py)

## Installation

To set up the project locally, follow these steps:

1. Clone the repository:
  ```bash
  git clone https://gitlab.com/dawdro/FirstClientServerApp.git
  ```

2. Navigate to the project directory:
  ```bash
  cd your-repo
  ```

## Usage

### Running the Server

#### Start the server:
  ```bash
  python echo_server.py
  ```
    or
  ```bash
  python echo_server_sqlite.py
  ```

#### Start the client:
  ```bash
  python echo-client.py
  ```

Follow the prompts to interact with the server using the implemented commands.

### Testing
To run the unit tests, use the following command:
```bash
  python -m unittest discover
```

There are two test files available one for PostgresSQL server version and one for SQLite version.

### Learning Principles
In order to maximize learning, the following principles were adhered to:

- No Copying Code: Avoid copying code from the internet or searching for ready-made solutions.

- Using Resources Wisely: Utilize Stack Overflow and documentation for tips or error information without copying solutions.

- Leveraging AI for Learning: Use tools like ChatGPT for explanations of concepts (e.g., socket workings, types of sockets and their configurations) but avoid requesting code solutions directly.

- Prompt Clarity: When using ChatGPT, always include "Do not give me code solutions, just explain it to me the simplest way or give me tips on how to solve the problem."

These principles ensure steady progress and a deeper understanding of the material.