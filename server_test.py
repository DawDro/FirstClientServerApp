import unittest
import echo_server


class TestServerCommands(unittest.TestCase):
    """To run the test you need to comment out self.server_start() line in echo_server.py in __init__"""

    def setUp(self):
        """Set up a Server instance for testing."""
        self.server = echo_server.Server()

    def test_show_data_db_for_admin(self):
        "The test is checking if show_data function returns correct data for the persons logged in"
        msg = {"command": "show_data", "username": "admin", "role": "admin"}
        result = self.server.show_data_db(msg)
        self.assertIsNotNone(result)

    def test_pull_user_data_from_db(self):
        "Check if method is pulling correct data for specified user"
        username = "user1"  # Type username
        result = self.server.pull_user_data_from_db(username)
        print(result)
        self.assertTrue(type(result) == tuple)

    def test_pull_user_data_from_db_for_nonexisting_user(self):
        "Check if method results None when user is not correct"
        username = "user2"  # Type username
        result = self.server.pull_user_data_from_db(username)
        print(result)
        self.assertIsNone(result)

    def test_pull_whole_data_from_db(self):
        "The test is checking if pull_whole_data_from_db function returns whole data from database"
        result = self.server.pull_whole_data_from_db()
        print(result)
        self.assertIsNotNone(result)

    def test_check_inbox_cap_db_for_user(self):
        "Check inbox capacity for specified user"
        username = "user1"
        result = self.server.check_inbox_cap_db(username)
        self.assertIsInstance(result[0], int)

    def test_user_login_correct_login_admin(self):
        login_msg = {"username": "admin1", "password": "admin_password"}
        expected = (
            "Login successful. Welcome, admin1!",
            "success",
            "admin",
            "admin1",
        )
        result = self.server.user_login_db(login_msg)
        self.assertEqual(result, expected)

    def test_user_login_with_wrong_password(self):
        wrong_password_msg = {"username": "admin1", "password": "adsadada"}
        expected = (
            "Invalid password. Please try again.",
            "failed",
            "na",
            "na",
        )
        result = self.server.user_login_db(wrong_password_msg)
        self.assertEqual(result, expected)

    def test_user_login_wrong_username_and_correct_password(self):
        wrong_username_msg = {"username": "adadada", "password": "admin_password"}
        expected = ("Username not found.", "failed", "na", "na")
        result = self.server.user_login_db(wrong_username_msg)
        self.assertEqual(result, expected)

    def test_save_message_when_user_inbox_full(self):
        """Test sending a message to a user with a full inbox."""
        message = {"username": "user1", "message": "Another message"}
        result = self.server.save_message_db(message)
        self.assertEqual(result, "User inbox is full, try again later")


if __name__ == "__main__":
    unittest.main()
