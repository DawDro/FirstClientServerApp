# echo-client.py
import socket
import json


class Client:
    """Represents a client that connects to the server and sends commands."""

    def __init__(self):
        self.addr = "127.0.0.1"
        self.port = 12345
        self.welcome_msg = "Hello Server! Client here!"

    def client_connect(self):
        """Connect to the server and handle communication."""
        try:
            self.initialize_connection()
            self.handshake_with_server()
            self.commands_loop()
        except socket.error as e:
            print(f"[Error] Socket error: {e}")
        except Exception as e:
            print(f"[Error] An unexpected error occurred: {e}")
        finally:
            self.disconnect()

    def initialize_connection(self):
        """Initialize the socket connection to the server."""
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.connect((self.addr, self.port))
        except socket.timeout:
            print(f"[Error] Connection to {self.addr}:{self.port} timed out.")
        except socket.error as e:
            print(f"[Error] Failed to connect to server: {e}")

    def handshake_with_server(self):
        """Perform handshake with server"""
        try:
            self.send_and_receive({"command": self.welcome_msg})
        except json.JSONDecodeError as e:
            print(f"[Error] Error decoding server response: {e}")
        except socket.error as e:
            print(f"[Error] Handshake failed due to a network error: {e}")

    def commands_loop(self):
        """Handle user commands in loop"""
        is_logged = False
        message = ""
        while message.lower() != "stop":
            message = input("Insert your command\n")
            if message.lower() == "login":
                if is_logged == True:
                    print(f"[Info] You are arleady logged in as {username}!")
                else:
                    credentials = self.get_credentials()
                    status = self.send_and_receive(credentials)
                    if status == "failed":
                        print("[Info] Log-in failed, try again")
                        break
                    else:
                        login_msg = self.check_login(status)
                        is_logged = login_msg[2]
                        username = login_msg[1]
                        role = login_msg[0]
            elif message.lower() == "send_msg":
                if is_logged == True:
                    self.send_msg_cmd()
                else:
                    print("[Info] You need to log in to send messages")
            elif message.lower() == "logout":
                if is_logged == True:
                    print("[Info] Succesfully logged out.")
                    is_logged = False
                else:
                    print("[Info] You are not logged in to log out.")
            elif message.lower() == "show_data":
                if is_logged == True:
                    self.show_data_cmd(username, role)
                else:
                    print("[Info] You need to log in to show data")
            else:
                self.send_and_receive({"command": message})

    def show_data_cmd(self, username, role):
        self.send_and_receive(
            {"command": "show_data", "username": username, "role": role}
        )

    def send_msg_cmd(self):
        self.msg_to_user = input("Please enter receiver username:\n")
        self.msg_to_send = input("Please enter your message:\n")
        self.send_and_receive(
            {
                "command": "send_msg",
                "username": self.msg_to_user,
                "message": self.msg_to_send,
            }
        )

    def get_credentials(self):
        """Prompting user for username and password"""
        while True:
            self.username = input("Please enter your username:\n")
            if self.username != "":
                break
            print("Uername cannot be empty.")
        while True:
            self.password = input(
                "Please enter your password (at least 8 characters long):\n"
            )
            if len(self.password) >= 8:
                break
            print("Password must be at least 8 characters long.")
        return {
            "command": "login",
            "username": self.username,
            "password": self.password,
        }

    def send_and_receive(self, msg):
        """
        Send a command to the server and handle the server's response.

        Args:
            msg (dict): The command to send, represented as a dictionary.
        """
        try:
            self.socket.send(json.dumps(msg).encode())

            # Receive and decode the server's JSON response
            response_data = self.socket.recv(1024)
            if response_data:
                self.msg_received = json.loads(response_data.decode())
                print(f"[Info] Server responds: {self.msg_received['command']}")
                return self.msg_received

        except (ConnectionResetError, ConnectionAbortedError):
            print("[Error] Connection lost. The server might have disconnected.")
            self.disconnect()
        except BrokenPipeError:
            print("[Error] Unable to send data to server (Broken pipe).")
            self.disconnect()
        except socket.error as e:
            print(f"[Error] Socket error occurred: {e}")
            self.disconnect()
        except Exception as e:
            print(f"[Error] An unexpected error occurred: {e}")
            self.disconnect()

    def check_login(self, msg):
        """
        Process the server's response to a login attempt and handle the outcome.

        Args:
            msg (dict): The server's response message, expected to include a "status" field
                        and optionally "username" and "role" fields.
        """

        try:
            received_msg = msg
            # Check if login successful
            if "status" in received_msg:
                if received_msg["status"] == "success":
                    print(
                        f"Hello {received_msg["username"]}, you have an {received_msg["role"]} rights.\n"
                    )
                    is_logged = True
                    logged_user_info = self.logged_user(
                        received_msg["role"], received_msg["username"], is_logged
                    )
                    return logged_user_info
                elif received_msg["status"] == "failed":
                    return "failed"

        except (ConnectionResetError, ConnectionAbortedError):
            print("[Error] Connection lost. Server might have stopped or disconnected.")
            self.disconnect()

        except BrokenPipeError:
            print("[Error] Broken pipe. Unable to send data to server.")
            self.disconnect()

        except json.JSONDecodeError:
            print("[Error] Failed to encode message as JSON.")

        except Exception as e:
            print(f"[Error] An unexpected error occurred: {e}")
            self.disconnect()

    def logged_user(self, role, username, is_logged):
        """Method that checks logged user, his role and catch logged user commands"""

        # Showing new commands related to logged user's role
        print(f"[Info] Welcome {username}! You are logged in as {role}.\n")
        if role == "admin":
            print(
                "Commands avaiable for admin are listed below:\n"
                "send_msg - send message to another user\n"
                "show_data - show users data\n"
                "logout - log out from user credentials"
            )
        elif role == "user":
            print(
                "Commands avaiable for user are listed below:\n"
                "send_msg - send message to another user\n"
                "show_data - show users data\n"
                "logout - log out from user credentials"
            )
        while is_logged == True:
            return role, username, is_logged

    def disconnect(self):
        """Server closing"""
        self.socket.close()


if __name__ == "__main__":
    client = Client()
    client.client_connect()
